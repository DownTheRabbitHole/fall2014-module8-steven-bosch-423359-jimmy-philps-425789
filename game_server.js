// Require the packages we will use:
var http = require("http"),
    mime = require('mime'),

    socketio = require("socket.io"),
    fs = require("fs"),
    url = require('url'),
    path = require('path'),
    path = require('path');
        
 var app = http.createServer(function(req, resp){
	var filename = path.join(__dirname, "static", url.parse(req.url).pathname);
	(fs.exists || path.exists)(filename, function(exists){
		if (exists) {
			fs.readFile(filename, function(err, data){
				if (err) {
					// File exists but is not readable (permissions issue?)
					resp.writeHead(500, {
						"Content-Type": "text/plain"
					});
					resp.write("Internal server error: could not read file");
					resp.end();
					return;
				}
 
				// File exists and is readable
				var mimetype = mime.lookup(filename);
				resp.writeHead(200, {
					"Content-Type": mimetype
				});
				resp.write(data);
				resp.end();
				return;
			});
		}else{
			// File does not exist
			resp.writeHead(404, {
				"Content-Type": "text/plain"
			});
			resp.write("Requested file not found: "+filename);
			resp.end();
			return;
		}
	});
});
app.listen(3456);

var gameRooms = {};         // room_name -> game_details
var waitingPlayers = {};    // player_name -> game_type
var players = {};           // player_name -> socket_id
var gameStats = {};         // player_name -> {game:stat} stat = {wins:num_wins, losses:num_losses, ties:num_ties}

var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
    
    socket.on("disconnect", function(data){
        for (var player in players){
            if (players[player] == socket.id) {
                var disconnected = player;
            }
        }
        if (disconnected) {
            io.sockets.emit("disconnect", {disconnected:disconnected});
            delete waitingPlayers[disconnected];
            for (var room in gameRooms){
                if (gameRooms[room].player1 == disconnected) {
                    var roomToDelete = room;
                    io.to(players[gameRooms[room].player2]).emit("reset", {msg:disconnected + " has disconnected."});
                }
                if (gameRooms[room].player2 == disconnected) {
                    var roomToDelete = room;
                    io.to(players[gameRooms[room].player1]).emit("reset", {msg:disconnected + " has disconnected."});
                }
            }
            delete gameRooms[roomToDelete];
        }
        
    });
    
    socket.on("leader", function(){
        var message = "";
        var scores = {ttt:[], chk:[], c4:[]};
        var ttt = [];
        var chk = [];
        var c4 = [];
        var counter = 0;
        for (var player in gameStats){
            ttt[counter] = player.ttt;
            ttt[counter].name = player;
            chk[counter] = player.chk;
            chk[counter].name = player;
            c4[counter] = player.c4;
            c4[counter].name = player;
            counter++;
        }
        ttt.sort(function(a, b){
            return (a.wins - a.losses) > (b.wins-b.losses);
        });
        chk.sort(function(a, b){
            return (a.wins - a.losses) > (b.wins-b.losses);
        });
        c4.sort(function(a, b){
            return (a.wins - a.losses) > (b.wins-b.losses);
        });
        io.to(socket.id).emit("leader", {ttt:ttt, chk:chk, c4:c4});
    });
    
    socket.on("register", function(data){
        var username = data.user;
        if (!players[username]) {
            players[username] = socket.id;
            io.to(socket.id).emit("register", {success: true});
            gameStats[username] = {ttt:{wins:0, losses:0}, chk:{wins:0, losses:0}, c4:{wins:0, losses:0}};
        }else{
            io.to(socket.id).emit("register", {success: false});
        }
    });
    
    socket.on("checkLogin", function(data){
       var user = data.user;
       if (!players[data]) {
            io.to(socket.id).emit("checkLogin", {success: false, user:user});
       }else{
            io.to(socket.id).emit("checkLogin", {success: true});
       }
    });
    
    // request to join game
    socket.on("joinRequest", function(data){
        var player = data.player;
        var game = data.game;
        var match = null;
        for (var user in waitingPlayers){
            if (waitingPlayers[user] == game) {
                // connect users
                var match = user;
                break;
            }
        }
        if (match) {
            delete waitingPlayers[match];
            var room = player+"_" +match+"_"+game;
            io.to(socket.id).emit("joinReply", {success: true, partner:match, room:room});
            io.to(players[match]).emit("joinReply", {success: true, partner:player, room:room});
            var dets = {game:game, player1:player, player2:match, player1Rematch:false, player2Rematch:false};
            dets.board = [];
            gameRooms[room] = dets;
        }else{
            waitingPlayers[player] = game;
            io.to(socket.id).emit("joinReply", {success: false});
        }
        
    });
    
    socket.on("rematch", function(data){
       var player = data.player;
       var room = data.room;
       if (gameRooms[room].player1 == player){
            gameRooms[room].player1Rematch = true;
       }else if (gameRooms[room].player2 == player) {
            gameRooms[room].player2Rematch = true;
       }
       if (gameRooms[room].player1Rematch && gameRooms[room].player2Rematch) {
            // New game
            gameRooms[room].player2Rematch = false;
            gameRooms[room].player1Rematch = false;
            gameRooms[room].board = [];
            io.to(room).emit("initGame", {game:gameRooms[room].game, firstTurn:gameRooms[room].player1});
       }
    });
    
    socket.on("joinRoom", function(data){
        socket.join(data.room);
        if (gameRooms[data.room].game == "chk") {
            var board = new Array();
            for (var i=0;i<8;i++) {
                board[i]=new Array();
                for (var j=0; j<8;j++) {
                    board[i][j]= new CheckerSquare(j, i, board);
                    if (i < 3 && (j+i)%2 == 1) {
                        board[i][j].player = 2;
                        board[i][j].isOccupied = true;
                    }else if (i > 4 && (i+j)%2 == 1) {
                        board[i][j].player = 1;
                        board[i][j].isOccupied = true;
                    }
                }
            }
            gameRooms[data.room].board = board;
        }
        if (gameRooms[data.room].game == "ConnectFour") { 
              for(var x = 0; x < 7; x++){
                     for(var y = 0; y < 6; y++){
                            gameRooms[data.room].board[getLocation(x,y)] = new Square(x,y,board);
                     }
              }
        }
        io.to(socket.id).emit("initGame", {game:gameRooms[data.room].game, firstTurn:gameRooms[data.room].player2});
    });
    
    socket.on("message", function(data){
        var user = data.user;
        var msg = data.msg;
        var room = data.room;
        io.to(room).emit("message", {message:msg, user:user});
    });
    
    socket.on("checkerMove", function(data){
       if (data.player == gameRooms[data.room].player1) {
         var player = 1;
       }else{
        var player = 2;
       }
       var room = data.room;
       var src = data.src.split("_");
       var dest = data.dest.split("_");
       if (player == 2) {
            var srcX = flipCoord(parseInt(src[0]));
            var srcY = flipCoord(parseInt(src[1]));
            var destX = flipCoord(parseInt(dest[0]));
            var destY = flipCoord(parseInt(dest[1]));
       }else{
            var srcX = parseInt(src[0]);
            var srcY = parseInt(src[1]);
            var destX = parseInt(dest[0]);
            var destY = parseInt(dest[1]);
       }
       
       if (gameRooms[room].board[srcY][srcX].player == player) {
            var move = gameRooms[room].board[srcY][srcX].move(destX, destY);
            if (move.success) {
                 for (var i=0; i<move.updates.length; i++){
                     var update = move.updates[i];
                     if (update.type == "move") {
                         io.to(room).emit("checkerUpdate",
                         {success:true, type:"move", player:data.player, xSrc:update.xSrc, xDest:update.xDest, ySrc:update.ySrc, yDest:update.yDest});
                     }else if (update.type == "jump") {
                         io.to(room).emit("checkerUpdate", {success:true, player:data.player, type:"jump", x:update.x, y:update.y});
                     }else if (update.type == "king") {
                         io.to(room).emit("checkerUpdate", {success:true, player:data.player, type:"king", x:update.x, y:update.y});
                     }else if (update.type == "win") {
                         io.to(room).emit("checkerUpdate", {success:true, player:data.player, type:"win", winner:update.winner});
                         gameStats[update.winner]["chk"].wins++;
                         if (gameRooms[room].player1 == update.winner) {
                            var loser = gameRooms[room].player2;
                         }else{
                            var loser = gameRooms[room].player1;
                         }
                         gameStats[loser]["chk"].losses++;
                     }
                 }
            }else{
                 io.to(socket.id).emit("checkerUpdate", {success:false, msg:move.msg});
            }
       }else{
            io.to(socket.id).emit("checkerUpdate", {success:false, msg:"Please select one of your pieces"});    
       }
    });
    
    function flipCoord(val) {
        return 7-val;
    }
    
    // TTT related
    socket.on("tttMove", function(data){
        var player = data.player;
        var move = parseInt((data.move).substring(3));
        if (!gameRooms[data.room].board[move]) {
            if (player == gameRooms[data.room].player1) {
                gameRooms[data.room].board[move] = 1;
            }else{
                gameRooms[data.room].board[move] = -1;
            }
            io.to(data.room).emit("tttUpdate", {move:move, player:player});
            var result = tttWinCond(data.room);
            var room = data.room;
            if (result == "player1") {
                // Player 1 has won.
                io.to(data.room).emit("tttWin", {winner:gameRooms[data.room].player1});
                gameStats[gameRooms[room].player1]["ttt"].wins++;
                gameStats[gameRooms[room].player2]["ttt"].losses++;
            }else if (result == "player2") {
                // Player 2 has won.
                io.to(data.room).emit("tttWin", {winner:gameRooms[data.room].player2});
                gameStats[gameRooms[room].player1]["ttt"].losses++;
                gameStats[gameRooms[room].player2]["ttt"].wins++;
            }else if (result) {
                io.to(data.room).emit("tttWin", {winner:"draw"});
            }
        }
    });
    
    
    socket.on("reset", function(data){
       var room = data.room;
       io.to(players[gameRooms[room].player1]).emit("reset", {});
       io.to(players[gameRooms[room].player2]).emit("reset", {});
       delete gameRooms[room];
    });
    
    socket.on("cfMove", function(data){
        var board = gameRooms[data.room].board;
        var player = data.player;
        var column = parseInt((data.move).substring(5)); // the whole column
        var row = getFirstEmptyHeight(column, board);
        var topRow = getTopRow(board);
        //var move = getTopRow(board)[column];
        if (/*move != null && */ !isFullCol(column, data.room)) {
            
            gameRooms[data.room].board[getLocation(column,row)].player = player;
            gameRooms[data.room].board[getLocation(column,row)].isFilled = true;
            gameRooms[data.room].board[getLocation(column,row)].isTopLevel = true;
            if (board[getLocation(column, row - 1)] != null) {
              board[getLocation(column, row - 1)].isTopLevel = false;
            }
            
            io.to(data.room).emit("cfUpdate", {column:column, row:row, /*move:move,*/ player:player});
            var result = cfWinCond(data.room);
            //console.log(result);
            if (result != false) { // only person who can win after a turn is the person who just moved
                io.to(data.room).emit("cfWin", {winner:player});
                gameStats[player]["c4"].wins++;
                if (gameRooms[data.room].player1 == player) {
                    var loser = gameRooms[data.room].player2;
                }else{
                    var loser = gameRooms[data.room].player1;
                }
                gameStats[loser]["c4"].losses++;
            }
            else if (isFull(board) && !result) {
                io.to(data.room).emit("cfWin", {winner:"draw"});
            }
        }
        else{
              console.log("Column full");
        }
    });
});

// TicTacToe
function tttWinCond(room){
    var board = gameRooms[room].board;
    var sums = [];
    var tie = true;
    sums[0] = board[1]+board[2]+board[3];
    sums[1] = board[4]+board[5]+board[6];
    sums[2] = board[7]+board[8]+board[9];
    sums[3] = board[1]+board[4]+board[7];
    sums[4] = board[2]+board[5]+board[8];
    sums[5] = board[3]+board[6]+board[9];
    sums[6] = board[1]+board[5]+board[9];
    sums[7] = board[3]+board[5]+board[7];
    for (var i=0; i<8; i++){
        if (sums[i] == 3) {
            return "player1";
        }else if (sums[i] == -3) {
            return "player2";
        }
        if (isNaN(sums[i])) {
            tie = false;
        }
    }
    return tie;
}

// Checkers
function CheckerSquare(x, y, board){
    this.board = board;
    this.x = x;
    this.y = y;
    this.isOccupied = false;
    this.player = null;
    this.isKing = false;
    this.validMove = function(xLoc, yLoc){
        console.log("entering valid move.");
        
        if (xLoc < 0 || xLoc > 7) {
            return false;
        }else if (yLoc < 0 || yLoc > 7) {
            return false;
        }else if (this.getChk(xLoc, yLoc).isOccupied) {
            return false;
        }else if (Math.abs(xLoc - this.x) > 2 || Math.abs(xLoc - this.x) < 1){
            return false;
        }else if (Math.abs(yLoc - this.y) > 2 || Math.abs(yLoc - this.y) < 1) {
            return false;
        }else if (Math.abs(xLoc - this.x) != Math.abs(yLoc - this.y)){
            return false;
        }else if (Math.abs(yLoc - this.y) == 2 && Math.abs(xLoc - this.x) == 2
                  && !this.getChk(((xLoc + this.x)/2), ((yLoc + this.y)/2)).isOccupied) {
                return false;
        }else if (Math.abs(yLoc - this.y) == 2 && Math.abs(xLoc - this.x) == 2
                  && this.getChk((xLoc + this.x)/2, (yLoc + this.y)/2).player == this.player) {
                return false;
        }else if (!this.isKing) {
            console.log("Info: player = " + this.player + " this.y = " + this.y + " yLoc= " +yLoc);
            if (this.player == 2) {
                if (yLoc < this.y) {
                    return false;
                }
            }else{
                if (yLoc > this.y) {
                    return false;
                }
            }
        }
        console.log("leaving valid move");
        return true;
    }
    
    
    this.getChk = function(xLoc, yLoc){
        return board[yLoc][xLoc];
    }
    
    this.move = function(xLoc, yLoc){
        console.log(this.x + "," + this.y + " -> "+xLoc + "," +yLoc);
        var msg = "";
        var updates = [];
        var counter = 0;
        console.log("calling valid move");
        if (this.validMove(xLoc, yLoc)) {
            var jumps = possibleJumps(this.board, this.player);
            if (jumps.length > 0) {
                var matchingJump = false;
                for (var i=0; i< jumps.length; i++){
                    console.log(jumps[i].xSrc + "," + jumps[i].ySrc + " "+jumps[i].xDest + ","+jumps[i].yDest);
                    if (jumps[i].xSrc == this.x && jumps[i].ySrc == this.y && jumps[i].xDest == xLoc && jumps[i].yDest == yLoc) {
                        matchingJump = true;
                    }
                }
                if (matchingJump) {
                    var jumpedX = (xLoc + this.x)/2;
                    var jumpedY = (yLoc + this.y)/2;
                    this.getChk(jumpedX, jumpedY).setJumped();
                    updates[counter] = {type:"jump", x:jumpedX, y:jumpedY};
                    counter++;
                    updates[counter] = {type:"move", xSrc:this.x, xDest:xLoc, ySrc:this.y, yDest:yLoc};
                    counter++;
                    
                    this.getChk(xLoc, yLoc).player = this.player;
                    this.getChk(xLoc, yLoc).isOccupied = true;
                    if (this.isKing) {
                        this.getChk(xLoc, yLoc).isKing = true;
                    }
                    this.player = null;
                    this.isOccupied = false;
                    this.isKing = false;
                    
                    if (this.kingMe()) {
                        this.isKing = true;
                        updates[counter] = {type:"king", x:this.x, y:this.y};
                        counter++;
                    }
                    var win = checkersWin(this.board);
                    if (win != 0) {
                        // game over
                        updates[counter] = {type:"win", winner:win};
                        counter++;
                    }
                }else{
                    msg = "You must jump if possible."
                }
            }else{
                updates[counter] = {type:"move", xSrc:this.x, xDest:xLoc, ySrc:this.y, yDest:yLoc};
                counter++;
                this.getChk(xLoc, yLoc).player = this.player;
                this.getChk(xLoc, yLoc).isOccupied = true;
                if (this.isKing) {
                        this.getChk(xLoc, yLoc).isKing = true;
                }
                this.isKing = false;
                this.player = null;
                this.isOccupied = false;
                if (this.kingMe()) {
                    this.isKing = true;
                    updates[counter] = {type:"king", x:this.x, y:this.y};
                    counter++;
                }
            }
        }else{
            msg = "Invalid move.";
        }
        var rtnObject = {updates:updates, msg:msg};
        if (msg == "") {
            rtnObject.success = true;
        }else{
            rtnObject.success = false;
        }
        return rtnObject;
    }
    
    this.setJumped = function(){
        this.isOccupied = false;
        this.player = null;
        this.isKing = false;
    }
    
    this.kingMe = function(){
        if (!this.isKing && this.y == 7) {
            return true;
        }else{
            return false;
        }
    }
    
    this.remainingMoves = function(){
        for (var i = this.x - 2; i <= this.x + 2; i++){
            for (var j = this.y - 2; j <= this.y + 2; j++){
                if (this.validMove(i,j)) {
                    return true;
                }
            }
        }
        return false;
    }
}

function checkersWin(board) {
    var moves1 = false;
    var moves2 = false;
    for (var i = 0; i < 8; i++){
        for (var j=0; j<8; j++){
            if (board[i][j].player == 1 && !moves1 && board[i][j].remainingMoves()) {
                moves1 = true;
            }
            if (board[i][j].player == 2 && !moves2 && board[i][j].remainingMoves()) {
                moves2 = true;
            }
        }
    }
    if (!moves1) {
        return 2;
    }else if (!moves2) {
        return 1;
    }else{
        return 0;
    }
}

function possibleJumps(board, player) {
    console.log("player "+ player);
    var moves = [];
    var counter = 0;
     for (var i = 0; i < 8; i++){
        for (var j=0; j<8; j++){
            if (board[i][j].player == player){
                for (var k = i - 2; k <= i + 2; k=k+4){
                    for (var l = j - 2; l<= j + 2; l=l+4){
                        if (board[i][j].validMove(l, k)) {
                            moves[counter] = {xSrc:j, ySrc:i, xDest:l, yDest:k};
                            counter++;
                        }
                    }
                }
            }
        }
     }
     return moves;
}

function cfWinCond(room) { 
       var board = gameRooms[room].board;
       var topRow = getTopRow(board);
       for(var x = 0; x < 7; x++){
              if (topRow[x] != null) {
                     var player = board[topRow[x]].player;
                     var upCheck = board[topRow[x]].checkWin("up", 0, board, player);
                     var downCheck = board[topRow[x]].checkWin("down", 0, board, player);
                     var leftCheck = board[topRow[x]].checkWin("left", 0, board, player);
                     var rightCheck = board[topRow[x]].checkWin("right", 0, board, player);
                     var upRightCheck = board[topRow[x]].checkWin("up right", 0, board, player);
                     var upLeftCheck = board[topRow[x]].checkWin("up left", 0, board, player);
                     var downRightCheck = board[topRow[x]].checkWin("down right", 0, board, player);
                     var downLeftCheck = board[topRow[x]].checkWin("down left", 0, board, player);
                     
                     if (upCheck != false) {
                            console.log("up");
                            return upCheck;
                     }
                     else if (downCheck != false) {
                            console.log("down");
                            return downCheck;
                     }
                     else if (leftCheck != false) {
                            console.log("left");
                            return leftCheck;
                     }
                     else if (rightCheck != false) {
                            console.log("right");
                            return rightCheck;
                     }
                     else if (upRightCheck != false) {
                            console.log("up right");
                            return upRightCheck;
                     }
                     else if (upLeftCheck != false) {
                            console.log("up left");
                            return upLeftCheck;
                     }
                     else if (downRightCheck != false) {
                            console.log("down right");
                            return downRightCheck;
                     }
                     else if (downLeftCheck != false) {
                            console.log("down left");
                            return downLeftCheck;
                     }
              }
       }
       return false; // HANDLE TIE CONDITION
}

function isFull(board) {
       for(var x = 0; x < 7; x++){
              for(var y = 0; y < 6; y++){
                     if (board[getLocation(x,y)] == null || board[getLocation(x,y)].isFilled == false) {
                            return false;
                     }
              }
       }
       return true;
}

function getLocation(x, y){ // calculates a key/index given x and y coordinates on table
       return (7 * y) + x; // 7 is the width of the board
}

function isFullCol(column, room){
       var board = gameRooms[room].board;
       for(var y = 0; y < 6; y++){
              if (board[getLocation(column, y)].isFilled == false) {
                     return false;
              }
       }
       return true;
}

function getFirstEmptyHeight(column, board){
       var counter = 0;
       while(counter < 6 && board[getLocation(column, counter)] != null && board[getLocation(column, counter)].isFilled){
              counter++;
       }
       return counter;
}

function findHighestFilled(column, board){
       var counter = -1;
       if (!board[getLocation(column, 0)].isFilled) {
              return -1;
       }
       for(var i = 0; i < 6; i++){
              if (!board[getLocation(column, i)].isFilled) {
                     break;
              }
              else{
                     counter++;
              }
       }
       return counter;
}


function getTopRow(board) { // returns an array of indeces in board that are the locations of the top elements in each column
       var returnable = [];
       for(var x = 0; x < 7; x++){
              if (findHighestFilled(x, board) != -1) {
                     var y = findHighestFilled(x, board);
                     returnable[x] = getLocation(x, y);
              }
              
       }
       return returnable;
}

function Square(x, y, board) {
       this.x = x;
       this.y = y;
       this.isFilled = false;
       this.player = null; // set when isFilled = true
       this.isTopLevel = false;
       this.checkWin = function (direction, num, board, player){
              if (direction == "up") {
                     if (board[getLocation(this.x, this.y + 3)] != null) {
                            if (board[getLocation(this.x, this.y+1)].player == this.player && board[getLocation(this.x, this.y+2)].player == this.player && board[getLocation(this.x, this.y+3)].player == this.player) {
                                   return player;
                            }
                     }
              }
              else if (direction == "down") {
                     if (board[getLocation(this.x, this.y - 3)] != null) {
                            if (board[getLocation(this.x, this.y-1)].player == this.player && board[getLocation(this.x, this.y-2)].player == this.player && board[getLocation(this.x, this.y-3)].player == this.player) {
                                   return player;
                            }
                     }
              }
              else if (direction == "left") {
                     if (board[getLocation(this.x - 3, this.y)] != null) {
                            if (board[getLocation(this.x-1, this.y)].player == this.player && board[getLocation(this.x-2, this.y)].player == this.player && board[getLocation(this.x-3, this.y)].player == this.player) {
                                   return player;
                            }
                     }
              }
              else if (direction == "right") {
                     if (board[getLocation(this.x + 3, this.y)] != null) {
                            if (board[getLocation(this.x+1, this.y)].player == this.player && board[getLocation(this.x+2, this.y)].player == this.player && board[getLocation(this.x+3, this.y)].player == this.player) {
                                   return player;
                            }
                     }
              }
              else if (direction == "up right") {
                     if (board[getLocation(this.x + 3, this.y + 3)] != null) {
                            if (board[getLocation(this.x+1, this.y+1)].player == this.player && board[getLocation(this.x+2, this.y+2)].player == this.player && board[getLocation(this.x+3, this.y+3)].player == this.player) {
                                   return player;
                            }
                     }
              }
              else if (direction == "up left") {
                     if (board[getLocation(this.x - 3, this.y + 3)] != null) {
                            if (board[getLocation(this.x-1, this.y+1)].player == this.player && board[getLocation(this.x-2, this.y+2)].player == this.player && board[getLocation(this.x-3, this.y+3)].player == this.player) {
                                   return player;
                            }
                     }
              }
              else if (direction == "down right") {
                     if (board[getLocation(this.x + 3, this.y - 3)] != null) {
                            if (board[getLocation(this.x+1, this.y-1)].player == this.player && board[getLocation(this.x+2, this.y-2)].player == this.player && board[getLocation(this.x+3, this.y-3)].player == this.player) {
                                   return player;
                            }
                     }
              }
              else if (direction == "down left") {
                     if (board[getLocation(this.x - 3, this.y - 3)] != null) {
                            if (board[getLocation(this.x-1, this.y-1)].player == this.player && board[getLocation(this.x-2, this.y-2)].player == this.player && board[getLocation(this.x-3, this.y-3)].player == this.player) {
                                   return player;
                            }
                     }
              }
              return false
       }
}




