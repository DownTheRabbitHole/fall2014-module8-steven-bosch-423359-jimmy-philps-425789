$(document).ready(function(){
    var socketio = io.connect();
    var myTurn = false;
    var userName = "";
    var partnerName = "";
    var room = "";
    var gameOver = false;
    var checkerSelect = [];
    var checkerFlip = false;
    
    $("#username").focus();
 
    $("#leader").click(function(data){
        socketio.emit("leader", {});
        
    });
    
    socketio.on("leader", function(data){
        $("#gameDiv").empty().load("leaderboard.html", function(){
            
            var chk = data.chk;
        var c4 = data.c4;
        var ttt=  data.ttt;
        alert(ttt.length);
        alert(c4.length);
        var tttTable = $("#tttTable");
        var c4Table = $("#c4Table");
        var chkTable = $("#chkTable");
        var headerRow = $("<tr></tr>");
        var rankHead = $("<th></th>").text("Rank");
        var nameHead = $("<th></th>").text("Username");
        var winHead = $("<th></th>").text("Wins");
        var lossHead = $("<th></th>").text("Losses");
        headerRow.append(rankHead).append(nameHead).append(winHead).append(lossHead);
        tttTable.append(headerRow);
        headerRow = $("<tr></tr>");
        rankHead = $("<th></th>").text("Rank");
        nameHead = $("<th></th>").text("Username");
        winHead = $("<th></th>").text("Wins");
        lossHead = $("<th></th>").text("Losses");
        headerRow.append(rankHead).append(nameHead).append(winHead).append(lossHead);
        c4Table.append(headerRow);
        headerRow = $("<tr></tr>");
        rankHead = $("<th></th>").text("Rank");
        nameHead = $("<th></th>").text("Username");
        winHead = $("<th></th>").text("Wins");
        lossHead = $("<th></th>").text("Losses");
        headerRow.append(rankHead).append(nameHead).append(winHead).append(lossHead);
        chkTable.append(headerRow);        
        for (var i = 0; i< 10; i++){
            if (ttt[i]) {
                var tttRow = $("<tr></tr>");
                var rankCell = $("<td></td>").text(i+1);
                tttRow.append(rankCell);
                var nameCell = $("<td></td>").text(ttt[i].name);
                tttRow.append(nameCell);
                var winCell = $("<td></td>").text(ttt[i].wins);
                tttRow.append(winCell);
                var lossCell = $("<td></td>").text(ttt[i].losses);
                tttRow.append(lossCell);
                tttTable.append(tttRow);
            }
            if (chk[i]) {
                var c4Row = $("<tr></tr>");
                var rankCell = $("<td></td>").text(i+1);
                c4Row.append(rankCell);
                var nameCell = $("<td></td>").text(c4[i].name);
                c4Row.append(nameCell);
                var winCell = $("<td></td>").text(c4[i].wins);
                 c4Row.append(winCell);
                 var  lossCell = $("<td></td>").text(c4[i].losses);
                c4Row.append(lossCell);
                chkTable.append(chkRow);
            }
            
            if (c4[i]) {
                    
                 var chkRow = $("<tr></tr>");
                 
                 var rankCell = $("<td></td>").text(i+1);
                 chkRow.append(rankCell);
                 
                 var nameCell = $("<td></td>").text(chk[i].name);
                 chkRow.append(nameCell);
                 
                 var winCell = $("<td></td>").text(chk[i].wins);
                 chkRow.append(winCell);
                 
                 var lossCell = $("<td></td>").text(chk[i].losses);
                 chkRow.append(lossCell);
                 c4Table.append(c4Row);
            }
        }
     });
    });
        
    
    function reset() {
        myTurn = false;
        partnerName = "";
        room = "";
        game = "";
        gameOver = false;
        $("#gameDiv").empty();
        $("#chatDiv").empty().prop("hidden", true);
    }
    
    socketio.on("reset", function(data){
        reset();
        if (data.msg) {
            alert(data.msg);
        }
    });
    
    socketio.on("checkLogin", function(data){
       if (data.success){
            $("#username").prop("hidden", true);
            $("#textheader").prop("hidden", true);
            $("#login").prop("hidden", true);
            userName = user;
            alert("Welcome, "+userName+"!");
       }else{
            alert("Username not found.");
       }
    });
    
    $("#login").click(function(data){
        var user = ($("#username").val()).trim();
        if (user) {
            var errorMsg = "";
             re = /^[a-zA-Z0-9]*$/;
            
            if (user != "") {
                var pMatch = user.match(re);
                if (pMatch) {
                    if (user.length > 20) {
                        errorMsg = "Username is too long.";
                    }
                }else{
                    errorMsg = "Username can only contain alphanumeric characters.";
                }
            }else{
                errorMsg = "Please enter a username.";
            }
            if (errorMsg != "") {
                alert(errorMsg);
            }else{
                socketio.emit("checkLogin", {user:user});
            }
        }
    });
    // Add user to game
    $("#joinGame").click(function(){
        
        var user = ($("#username").val()).trim();
        if (user) {
            var game = $("#gameSel").val();
            socketio.emit("joinRequest", {player:user, game:game});
            userName = user;
            $("#msg").prop("hidden", true);
            
            $("#gameSel").prop("hidden", true);
            $("#joinGame").prop("hidden", true);
          $("#textheader").prop("hidden", true);
          $("#regHeader").prop("hidden", true);
          $("#register").prop("hidden", true);
          $("#leader").prop("hidden", true);


        }
    });
    
    socketio.on("message", function(data) {
        //Append an HR thematic break and the escaped HTML of the new message
        $("#chatlog").append($("<hr>"));
        var message = "";
        if (data.user) {
            message += data['user'] + ": ";
        }
        if (data.message) {
            message += data.message;
        }
        var node = $("<p>"+message+"</p>");
        $("#chatlog").append(node);
        $("#chatlog").animate({scrollTop: $("#chatlog").prop("scrollHeight") - $("#chatlog").height()}, "fast");
    });
    
    
    socketio.on("joinReply", function(data){
       if (data.success){
            partnerName = data.partner;
            room = data.room;
            socketio.emit("joinRoom", {room:room});
            $("#chatDiv").empty().prop("hidden", false).load("chat.html", function(){
                $("#msg").click(function(){
                    var msg = $("#message_input").val();
                    socketio.emit("message", {user:userName, msg:msg, room:room});
                    $("#message_input").val("");
                });
                $("#headerText").text("Chat with "+partnerName+"!");
            });
            
       }else{
            $("#gameDiv").empty().load("waiting.html", function(){});
           
       }
    });
    
    socketio.on("initGame", function(data){
       if (data.game == "ttt") {
        //Tic tac toe
            initTTT(data.firstTurn);
       }else if (data.game == "chk") {
            initCheckers(data.firstTurn);
       }else if (data.game == "ConnectFour") {
            initCF(data.firstTurn);
       }
       
       if (data.firstTurn == userName) {
            myTurn = true;
       }else{
            myTurn = false;
       }
       
    });
    
    function initTTT(first) {
        $("#gameDiv").empty().load("tictactoe.html", function(){
            var tttBoard = $("#tttBoard");
            var rows = tttBoard.find("tr");
            var row = rows.first();
            for (var i = 0; i < rows.length; i++){
                var cells = row.children();
                var cell = cells.first();
                for (var j = 0; j<cells.length; j++){
                    cell.click(tttMove);
                    cell = cell.next();
                }
                row = row.next();
            }
            $("#tttDisp").text(first + "'s turn");
            
            $("#yes").click(function(){
                 $("#tttDisp").text("Waiting for " + partnerName + " to accept.");
                socketio.emit("rematch", {room:room, player:userName});
            });
            
            $("#no").click(function(){
                socketio.emit("reset", {room:room});
            });
            
            $("#yes").prop("hidden", true);
            $("#no").prop("hidden", true);
        });
    }
  
    function initCheckers(first){
        $("#gameDiv").empty().load("checkers.html", function(){
            var checkerBoard = $("#checkerBoard");
            for (var i=0; i<8; i++){
                var row = $("<tr></tr>");
                for (var j=0; j<8; j++){
                    var square = $("<td></td>").click(checkerMove).prop("id", j+"_"+i);
                    if ((j+i)%2 == 0) {
                        square.css("background-color", "red");
                    }else{
                        square.css("background-color", "black");
                    }
                    if (first == userName) {
                        checkerFlip = true;
                        if (i < 3 && (j+i)%2 == 1) {
                            var img = $("<img>").prop("src", "kanye1.png");
                            square.append(img);
                        }else if (i > 4 && (i+j)%2 == 1) {
                            var img = $("<img>").prop("src", "kanye2.png");
                            square.append(img);
                        }
                    }else{
                        if (i < 3 && (j+i)%2 == 1) {
                            var img = $("<img>").prop("src", "kanye2.png");
                            square.append(img);
                        }else if (i > 4 && (i+j)%2 == 1) {
                            var img = $("<img>").prop("src", "kanye1.png");
                            square.append(img);
                        }
                    }
                    row.append(square);
                }
                checkerBoard.append(row);
            }
            $("#tttDisp").text(first + "'s turn");
            $("#yes").click(function(){
                 $("#tttDisp").text("Waiting for " + partnerName + " to accept.");
                socketio.emit("rematch", {room:room, player:userName});
            });
            
            $("#no").click(function(){
                socketio.emit("reset", {room:room});
            });
            
            $("#yes").prop("hidden", true);
            $("#no").prop("hidden", true);
        
        });
    }
         
    socketio.on("register", function(data){
        if (data.success) {
            alert("Congratulations!");
            $("#textheader").prop("hidden", false);
            $("#username").prop("hidden", false);
            $("#regHead").prop("hidden", true);
            $("#newName").prop("hidden", true);
            $("#registerNow").prop("hidden", true);
            
            
        }else{
            alert("Username is already taken.");
        }
    });        
    socketio.on("checkerUpdate", function(update){
        if (update.success) {
            var player = update.player;
            if (update.type == "move") {
                if (checkerFlip) {
                    var xSrc = flipCoord(update.xSrc);
                    var ySrc = flipCoord(update.ySrc);
                    var xDest = flipCoord(update.xDest);
                    var yDest = flipCoord(update.yDest);
                }else{
                    var xSrc = update.xSrc;
                    var ySrc = update.ySrc;
                    var xDest = update.xDest;
                    var yDest = update.yDest;   
                }
                var img = $("#" + xSrc +"_"+ySrc).find("img").first();
                var newImg = $("<img>").prop("src", img.prop("src"));
                $("#"+xDest+"_"+yDest).append(newImg);
                $("#" + xSrc +"_"+ySrc).empty();
                myTurn = !myTurn;
            }else if (update.type == "jump") {
                if (checkerFlip) {
                    var x = flipCoord(update.x);
                    var y = flipCoord(update.y);
                }else{
                    var x = update.x;
                    var y = update.y;
                }
                $("#"+x+"_"+y).empty();
                
            }else if (update.type == "king") {
                if (checkerFlip) {
                    var x = flipCoord(update.x);
                    var y = flipCoord(update.y);
                }else{
                    var x = update.x;
                    var y = update.y;
                }
               // var img = $("#" + x +"_"+y).find("img").first();
                //var newImg = $("<img>").prop("src", "crown"+img.prop("src"));
               // $("#" + x +"_"+y).empty().append(newImg);
            }else if (update.type == "win") {
                var winner = update.winner;
            }
            
            if (myTurn) {
                $("#tttDisp").text("It's " +userName + "'s turn.");
            }else{
                $("#tttDisp").text("It's "+partnerName + "'s turn.");
            }
        }else{
            alert(update.msg);
        }
       
    });
    
    function checkerMove(event){
        if (myTurn && !gameOver) {
            if (checkerSelect.length == 0) {
                checkerSelect[0] = $(this).prop("id");
                $(this).css("border", "solid #6699FF 4px");
            }else if (checkerSelect.length == 1) {
                if ($(this).prop("id") == checkerSelect[0]) {
                    checkerSelect = [];
                    $(this).css("border", "none");
                }else{
                    checkerSelect[1] = $(this).prop("id");
                    socketio.emit("checkerMove", {player:userName, src:checkerSelect[0], dest:checkerSelect[1], room:room});
                    $("#" + checkerSelect[0]).css("border", "none");
                    checkerSelect = [];
                }
            }
        }
    }
    
    function flipCoord(val) {
        return 7-val;
    }
    
    function tttMove(event) {
        if (myTurn && !gameOver) {
            socketio.emit("tttMove", {player:userName, move:$(this).prop("id"), room:room});
        }
    }
    
    socketio.on("tttWin", function(data){
        gameOver = true;
        var winner = data.winner;
        if (winner == "draw") {
            $("#tttDisp").text("It's a Draw!");
        }else{
            $("#tttDisp").text(winner + " is the winner!");
        }
        $("#tttYes").prop("hidden", false);
        $("#tttNo").prop("hidden", false);
    });
    
    socketio.on("tttUpdate", function(data){
        myTurn = !myTurn;
        var move = data.move;
        var player = data.player;
        var cell = $("#ttt" +move);
        if (myTurn) {
            $("#tttDisp").text("It's " +userName + "'s turn.");
        }else{
            $("#tttDisp").text("It's "+partnerName + "'s turn.");
        }
        if (data.player == userName) {
            cell.text("X");
        }else{
            cell.text("O");
        }
    });
    
    function initCF(first) {
        $("#gameDiv").empty().prop.load("connectfour.html", function(){
            var cfBoard = $("#cfBoard");
            var rows = cfBoard.find("tr");
            var row = rows.first();
            for (var i = 0; i < rows.length; i++){
                var cells = row.children();
                var cell = cells.first();
                for (var j = 0; j<cells.length; j++){
                    cell.click(cfMove);
                    cell = cell.next();
                }
                row = row.next();
            }
            $("#cfDisp").text(first + "'s turn");
            
            $("#yes").click(function(){

            });
            
            $("#no").click(function(){

            });
        });
    }
    
    function cfMove(event) {
        if (myTurn && !gameOver) {
            socketio.emit("cfMove", {player:userName, move:$(this).prop("id"), room:room});
        }
    }
    
    $("#register").click(function(){
        $("#regHeader").prop("hidden", true);
        $("#textheader").prop("hidden", true);
        $("#username").prop("hidden", true);
        $("#register").prop("hidden", true);
        $("#regHead").prop("hidden", false);
        $("#newName").prop("hidden", false);
        $("#registerNow").prop("hidden", false);
        $("#login").prop("hidden", true);
    });
    
    $("#registerNow").click(function(){
        var user = $("#newName").val();
        if (user) {
            var errorMsg = "";
            re = /^[a-zA-Z0-9]*$/;
            
            if (user != "") {
                var pMatch = user.match(re);
                if (pMatch) {
                    if (user.length > 20) {
                        errorMsg = "Username is too long.";
                    }
                }else{
                    errorMsg = "Username can only contain alphanumeric characters.";
                }
            }else{
                errorMsg = "Please enter a username.";
            }
            if (errorMsg != "") {
                alert(errorMsg);
            }else{
                socketio.emit("register", {user:user});
            }
        }
    });
    
    socketio.on("cfWin", function(data){
        var winner = data.winner;
        if (winner == "draw") {
            $("#cfDisp").text("Draw");
        }else{
            $("#cfDisp").text(winner + " is the winner!");
        }
        gameOver = true;
    });
    
    socketio.on("cfUpdate", function(data){
        myTurn = !myTurn;
        var move = data.move;
        var player = data.player;
        var column = data.column;
        var row = data.row;
        var cell = $("#cfr" + row + "c" + column);
        if (myTurn) {
            $("#cfDisp").text(userName + "'s turn");
        }else{
            $("#cfDisp").text(partnerName + "'s turn");
        }
        if (data.player == userName) {
            cell.text("X"); 
        }else{
            cell.text("O");
        }
    });
});