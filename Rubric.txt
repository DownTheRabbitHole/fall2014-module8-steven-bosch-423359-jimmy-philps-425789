Jimmy Philps - 425789
Steven Bosch - 423359

5 - Rubric
10 - Best Practices / Usability
35 - using js/socket.io effectively for multiplayer gaming
   5 - users can register / login
   15 - multiple users can join lobby / wait for players.
   5 - leaderboard on each game / compare scores with friends.
   10 - users can IM each other during games.
20 - checkers is fully functional
15 - connect 4 is fully functional
15 - tic tac toe is fully functional

